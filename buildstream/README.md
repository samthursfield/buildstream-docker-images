Published tags and respective `Dockerfile` links
------------------------------------------------

* `latest` ([latest.Dockerfile](https://gitlab.com/BuildStream/buildstream-docker-images/blob/master/buildstream/latest.Dockerfile)) [![](https://images.microbadger.com/badges/image/buildstream/buildstream.svg)](https://microbadger.com/images/buildstream/buildstream)
* `dev` ([dev.Dockerfile](https://gitlab.com/BuildStream/buildstream-docker-images/blob/master/buildstream/dev.Dockerfile)) [![](https://images.microbadger.com/badges/image/buildstream/buildstream:dev.svg)](https://microbadger.com/images/buildstream/buildstream:dev)
* `nightly` ([nightly.Dockerfile](https://gitlab.com/BuildStream/buildstream-docker-images/blob/master/buildstream/nightly.Dockerfile)) [![](https://images.microbadger.com/badges/image/buildstream/buildstream:nightly.svg)](https://microbadger.com/images/buildstream/buildstream:nightly)
* `latest-slim` ([latest-slim.Dockerfile](https://gitlab.com/BuildStream/buildstream-docker-images/blob/master/buildstream/latest-slim.Dockerfile)) [![](https://images.microbadger.com/badges/image/buildstream/buildstream:latest-slim.svg)](https://microbadger.com/images/buildstream/buildstream:latest-slim)
* `dev-slim` ([dev-slim.Dockerfile](https://gitlab.com/BuildStream/buildstream-docker-images/blob/master/buildstream/dev-slim.Dockerfile)) [![](https://images.microbadger.com/badges/image/buildstream/buildstream:dev-slim.svg)](https://microbadger.com/images/buildstream/buildstream:dev-slim)
* `nightly-slim` ([nightly-slim.Dockerfile](https://gitlab.com/BuildStream/buildstream-docker-images/blob/master/buildstream/nightly-slim.Dockerfile)) [![](https://images.microbadger.com/badges/image/buildstream/buildstream:nightly-slim.svg)](https://microbadger.com/images/buildstream/buildstream:nightly-slim)
* `latest-extra` ([latest-extra.Dockerfile](https://gitlab.com/BuildStream/buildstream-docker-images/blob/master/buildstream/latest-extra.Dockerfile)) [![](https://images.microbadger.com/badges/image/buildstream/buildstream:latest-extra.svg)](https://microbadger.com/images/buildstream/buildstream:latest-extra)
* `dev-extra` ([dev-extra.Dockerfile](https://gitlab.com/BuildStream/buildstream-docker-images/blob/master/buildstream/dev-extra.Dockerfile)) [![](https://images.microbadger.com/badges/image/buildstream/buildstream:dev-extra.svg)](https://microbadger.com/images/buildstream/buildstream:dev-extra)
* `nightly-extra` ([nightly-extra.Dockerfile](https://gitlab.com/BuildStream/buildstream-docker-images/blob/master/buildstream/nightly-extra.Dockerfile)) [![](https://images.microbadger.com/badges/image/buildstream/buildstream:nightly-extra.svg)](https://microbadger.com/images/buildstream/buildstream:nightly-extra)


Supported Platforms
-------------------

Each of the above tags are supported on the following platforms:

* Linux - `x86_64`
* Linux - `aarch64`


What is BuildStream
-------------------

BuildStream is a Free Software tool for integrating software stacks.

It takes inspiration, lessons and use-cases from various projects including
Bazel, OBS, Reproducible Builds, Yocto, Baserock, Buildroot, Aboriginal, GNOME
Continuous, JHBuild, Flatpak Builder and Android repo.

BuildStream supports multiple build-systems (e.g. autotools, cmake, cpan,
distutils, make, meson, qmake, pip) through its core plugins. External plugins
allow the creation of a range of output formats (e.g. debian packages, flatpak
runtimes, system images) for multiple platforms and chipsets.

[https://buildstream.build](https://buildstream.build)

[BuildStream Documentation](https://docs.buildstream.build/)


How to use this image
---------------------

For detailed instructions on how to use BuildStream via Docker, see
[USING.md](https://gitlab.com/BuildStream/buildstream-docker-images/-/blob/master/USING.md).


Image Variants
--------------

The `buildstream/buildstream` image comes in many flavors, each designed for
a specific use-case. All of the following tags are rebuilt on a daily basis.

* `latest`: This tag tracks the latest stable release of BuildStream. It will
  also contain the system base dependencies of all the Core plugins of
  BuildStream.

* `dev`: This tag tracks the latest development snapshot of BuildStream.
  It will also contain the system base dependencies of all the Core plugins of
  BuildStream.

* `nightly`: This tag tracks the master branch of BuildStream.  It will also
  contain the system base dependencies of all the Core plugins of BuildStream.

* `latest-slim`: Similar to `latest`, except that it does not contain the
   dependencies of any plugins.

* `dev-slim`: Similar to `dev`, except that it does not contain the
  system base dependencies of any plugins.

* `nightly-slim`: Similar to `nightly`, except that it does not contain the
  system base dependencies of any plugins.

* `latest-extra`: Similar to `latest`, but it also contains external plugins
  and their system base dependencies.

* `dev-extra`: Similar to `dev`, but it also contains external plugins
  and their system base dependencies.

* `nightly-extra`: Similar to `nightly`, but it also contains external plugins
  and their system base dependencies.


Where to raise issues
---------------------

This image is generated by the
[buildstream/buildstream-docker-images](https://gitlab.com/BuildStream/buildstream-docker-images)
project on GitLab. Merge requests and issue reports for the image itself should
be filed against that project.

Issue reports for BuildStream itself should be filed against the
[buildstream/buildstream](https://github.com/apache/buildstream) project
on GitLab.
